# 문제와 해결

### 1. COR (Cross-Origin-Resource)
  - JSONP로 해결을 시도했으나 `callback` 함수에 전달이 안되는 문제가 있었음 (서버에서 막아둔 것으로 예상)
  - 크롬 확장 프로그램 Allow-Control-Allow-Origin을 사용하여 COR 이슈를 우회

### 2. React
  - React Native를 이전에(1년전) 사용해 보았으나 React는 익숙하지 않았음
  - 튜토리얼과 블로그 등을 참고하여 코드 구성을 완성
  - 구현 관련 설명
    - EvnetContainer: 스크롤 이벤트를 통해 기본 이미지(8장) 외의 이미지는 스크롤 되는 중에 `visibleLength` 값을 조정하여 지연 평가되도록 구현
    - EventItem: 총 그려야할 이미지 수량(`visibleLength`)에 따라 배너 이미지를 로드함
    - Header: menu state의 `is_active` 값을 변경(setState)하면서 Body의 `props.contents`를 변경할 수 있음
    - Body: Header의 menu 값의 변경되는 것에 따라 다양한 정보를 담을 수 있음

### 3. Redux
  - 마찬가지로 익숙하지 않은 라이브러리
  - State 관리를 외부로 컴포넌트 간의 Props로 소통하는 계층 구조에서 비롯되는 불편한 상태관리를 해결하기 위한 도구라고 판단됨
  - 외부에 Store를 두고 상태가 변경되면 Store에 `dispatch` 함수를 호출하여 상태를 변경하고
  - 상태의 변경에 따라 특정 반응을 보여야하는 컴포넌트는 해당 상태를 구독(`subscribe`)함으로써 변경이 필요한 시기에 변경이 가능해짐
  - 하지만 _이번 구현에서 Redux를 사용하는 것은 불필요한 스펙이라고 판단하여 사용을 배제함_

### 4. CSS
  - 이벤트 배너 이미지(`.img.banner`)와 하단의 설명부(`.card_footer`) 사이 간격 이슈가 있었음
  - a 태그로 감싼 내용의 요소들 간에 자동으로 할당되는 간격(blank)가 있었음
  - wrapper에 해당하는 a 태그에 flex 속성을 부여하는 것으로 해결
