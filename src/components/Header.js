import React, { Component } from 'react';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menu: [
        { id: 1, name: '홈', path: '/home', is_active: false },
        { id: 2, name: '인기', path: '/pop', is_active: false },
        { id: 3, name: '신규', path: '/new', is_active: false },
        { id: 4, name: '시술별', path: '/category', is_active: false },
        { id: 5, name: '기획전', path: '/curation', is_active: true }
      ]
    }
  }

  render() {
    return (
      <div className="header">
        <div className="navigation">
          <div className="logo"/>
          <div className="controls">
            <div className="search"/>
            <div className="wish_list"/>
          </div>
        </div>
        <div className="menu_bar">
          {this.state.menu.map(m => (
            <a href={m.path} key={m.id} className={m.is_active ? 'active' : ''}>
              <span>{m.name}</span>
            </a>)
          )}
        </div>
      </div>
    )
  }
}

export default Header;