import React from 'react';

const img_or_blank = ({ mainImage: { url }, title }, cond) => cond ?
  <img src={url} alt={title} className="img banner"/> :
  <div className="blank banner"/>;

const EventItem = ({ item, idx, visibleLength }) => (
  <li className="card" idx={idx}>
    <a href={`/curation/${item.groupId._id}`} className="wrapper">
      {img_or_blank(item, idx <= visibleLength)}
      <div className="card_footer">
        <h5 className="title">{item.title}</h5>
        <div className="sub_title">
          <span className="menu_title">기획전</span>
          <span className="partition"> | </span>
          <span className="description">{item.groupId.description}</span>
        </div>
      </div>
    </a>
  </li>
);

export default EventItem;