import React from 'react';

const Body = props => (
  <div className="content">
    {props.contents}
  </div>
);

export default Body;